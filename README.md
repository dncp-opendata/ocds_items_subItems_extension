# Extensión de Sub Items

En Paraguay, las convocantes pueden dividir los items de la convocatoria en sub
items, para permitir mayor especificidad en los mismos.

Esta extensión agrega el array de `subItems` del tipo `Item` al objeto `items`
para permitir la publicación de esta información. 

También, agrega el campo
`group` al objeto `Item` dado que en algunos casos un Sub Item sirve solo de 
"padre" para agrupar subitems relacionados.

Si un subitem no tiene agrupador, entonces el campo `group` tendrá el mismo valor
que el campo `description` para mantener la consistencia.

## Ejemplo

```json
"items": [
    {
        "id": "7ACmxCrNDFM=",
        "description": "Pavimento Tipo Empedrado",
        "additionalClassifications": [
            {
                "id": "72131701",
                "description": "Pavimentación,construccion, arreglo o hermoseamiento de carreteras o caminos",
                "uri": "https://www.contrataciones.gov.py/datos/api/v3/doc/catalogo/nivel-4/72131701",
                "scheme": "UNSPSC"
            }
        ],
        "classification": {
            "id": "72131701-002",
            "description": "Construcción de empedrado",
            "uri": "https://www.contrataciones.gov.py/datos/api/v2/doc/catalogo/nivel-5/72131701-002",
            "scheme": "x_catalogoNivel5DNCP"
        },
        "unit": {
            "name": "Unidad Medida Global",
            "id": "GL",
            "value": {
                "currency": "PYG",
                "amount": 35282050
            }
        },
        "quantity": 1,
        "subItems": [
            {
                "id": "w2tPigVdJuk=",
                "description": "4.2 Colocación de piedra (incluye la maceada de la piedra)",
                "group": "4.2-Colocación de piedra (incluye la maceada de la piedra)",
                "unit": {
                    "name": "Metros cuadrados",
                    "id": "M2",
                    "value": {
                        "currency": "PYG",
                        "amount": "16821000"
                    }
                }
            }
        ]
    }
]
```


## Issues

Report issues for this extension in the [ocds-extensions repository](https://github.com/open-contracting/ocds-extensions/issues), putting the extension's name in the issue's title.

## Changelog

This extension was originally discussed in <https://github.com/open-contracting/standard/issues/899>.
